from django.shortcuts import render
from .models import Project

# Create your views here.

# If you need to do something to the model before saving,
# you can get the instance by calling
# model_instance = form.save(commit=False)
# Modifying the model_instance
# and then calling model_instance.save()


def list_projects(request):
    list_projects = Project.objects.all()
    context = {"list_projects": list_projects}
    return render(request, "projects/list.html", context)
